# # Topics
#
# * strings and numbers
# * modules
# * reopening classes
#
# # Extending a built-in class
#
# To make this test pass you'll need to extend a built-in
# class. You'll see that we're creating a new spec for 'Fixnum' --
# this isn't a new class you'll be building, but instead it is a
# built-in class you will extend.
#
# Remember that in Ruby, everything is an object, even a number.  Try
# this in irb:
#
#     >> 4.class
#     => Fixnum
#     >> 4.methods
#     \["inspect", "%", "<<", ...
#
# The number 4 is of class `FixNum` and it has methods on it.  Your
# challenge is to add an `in_words` method to `FixNum`.

class Fixnum

  def number_to_words
    number_words_library = {
        1 => "one", 2 => "two", 3 => "three",
        4 => "four", 5 => "five", 6 => "six", 7 => "seven",
        8 => "eight", 9 => "nine", 10 => "ten", 11 => "eleven",
        12 => "twelve", 13 => "thirteen", 14 => "fourteen",
        15 => "fifteen", 16 => "sixteen", 17 => "seventeen",
        18 => "eighteen", 19 => "nineteen", 20 => "twenty",
        30 => "thirty", 40 => "forty", 50 => "fifty", 60 => "sixty",
        70 => "seventy", 80 => "eighty", 90 => "ninety",
        # 100 => "hundred", 1000 => "thound", 1000000 => "million",
        # 1000000000 => "billion", 1000000000000 => "trillions"
    }
  end

  def in_words
    number = self
    return "zero" if number == 0
    to_word(number)
  end

private

  def to_word(number)
    word = []
    if has_keys?(number) && number < 100
      word << number_to_words[number]
    elsif number.to_s.length <= 3
      word << three_digits(number)
    elsif number.to_s.length.between?(4, 7)
      word << thousands(number)
    elsif number >= 1_000_000 && number < 1_000_000_000
      word << millions(number)
    elsif number >= 1_000_000_000 && number < 1_000_000_000_000
      word << billions(number)
    elsif number >= 1_000_000_000_000
      word << trillions(number)
    end
    word.join(" ")
  end

  def has_keys?(number)
    number_to_words.has_key?(number)
  end

  def two_digits(number)
    convert = []
    if has_keys?(number)
      convert << number_to_words[number]
    else
    ones = number % 10
    tenth = number % 100 - ones
    convert << number_to_words[tenth] << number_to_words[ones]
    end
    convert
  end

  def three_digits(number)
    convert = []
    hundreds = (number % 1000 - number % 100)
    convert << number_to_words[hundreds / 100] +" hundred" unless hundreds == 0
    convert << two_digits(number - hundreds) if (number - hundreds) > 0
    convert
  end

  def thousands(number)
    convert = []
    thousand = number % 1000000 - number % 1000
    convert << three_digits(thousand / 1000) << "thousand" unless thousand == 0
    convert << three_digits(number - thousand) if (number - thousand) > 0
    convert
  end

  def millions(number)
    convert = []
    million = number % 1000000000 - number % 1000000
    convert << three_digits(million / 1000000) << "million" unless million == 0
    convert << thousands(number - million) if (number - million) > 0
    convert
  end

  def billions(number)
    convert = []
    billion = number % 1000000000000 - number % 1000000000
    convert << three_digits(billion / 1000000000) << "billion" unless billion == 0
    convert << millions(number - billion) if (number - billion) > 0
    convert
  end

  def trillions(number)
    convert = []
    trillion = number % 1000000000000000 - number % 1000000000000
    convert << three_digits(trillion / 1000000000000) << "trillion" unless trillion == 0
    convert << billions(number - trillion) if (number - trillion) > 0
    convert
  end

end
